<?php
/**
 * @global $APPLICATION CMain
 */
include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use \Stratosfera\Core\Helpers\Iblock;

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$get = $request->getQueryList()->toArray();

if ($get['item']) {
    $IB_WORKERS_ID = Iblock::getIblockIdByCode(IB_CODE_WORKERS);
    $arWorker = CIBlockelement::GetList(
        [],
        [
            'ID' => $get['item'],
            'IBLOCK_ID' => $IB_WORKERS_ID
        ],
        false,
        false,
        ['ID', 'NAME', 'PROPERTY_position']
    )->fetch();
}
?>

<div class="w-25">
    <h4 class="modal__title">Исполнитель</h4>
    <form id="workerForm" method="post" action="">
        <?php echo ($arWorker['ID']) ? '<input type="hidden" name="item" value="'.$arWorker['ID'].'">' : ''; ?>
        <div class="form-group">
            <label for="workerName">Имя</label>
            <input type="text" class="form-control" id="workerName" placeholder="Иван" name="name" value="<?= $arWorker['NAME']; ?>">
        </div>
        <div class="form-group">
            <label for="workerPosition">Должность</label>
            <input type="text" class="form-control" id="workerPosition" placeholder="Программист" name="position" value="<?= $arWorker['PROPERTY_POSITION_VALUE']; ?>">
        </div>
        <div class="form-group">
            <p class="form-text text-danger form-text-error"></p>
        </div>
        <button type="submit" class="btn btn-primary workerSubmit">Сохранить</button>
    </form>
</div>