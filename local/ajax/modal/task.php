<?php
/**
 * @global $APPLICATION CMain
 */
include($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');
use \Stratosfera\Core\Helpers\Iblock;
use Bitrix\Main\Loader;
Loader::IncludeModule('iblock');

$request = \Bitrix\Main\Context::getCurrent()->getRequest();
$get = $request->getQueryList()->toArray();

if ($get['item']) {
    $IB_TASKS_ID = Iblock::getIblockIdByCode(IB_CODE_TASKS);
    $obTask = CIBlockElement::GetList(
        [],
        [
            'ID' => $get['item'],
            'IBLOCK_ID' => $IB_TASKS_ID
        ]
    )->GetNextElement();
    $arTask['FIELDS'] = $obTask->getFields();
    $arTask['PROPS'] = $obTask->getProperties();
}

$arStatus = [];
$property_enums = CIBlockPropertyEnum::GetList(
    [],
    [
        'CODE' => PROP_CODE_STATUS,
        'IBLOCK_ID' => $IB_TASKS_ID
    ]
);
while ($arRes = $property_enums->fetch()) {
    $arStatus[$arRes['ID']] = $arRes;
}

$arWorkers = getWorkersInfo();
?>

<div class="w-25">
    <h4 class="modal__title">Задача</h4>
    <form id="taskForm" method="post" action="">
        <?php echo ($arTask['FIELDS']['ID']) ? '<input type="hidden" name="item" value="'.$arTask['FIELDS']['ID'].'">' : ''; ?>
        <div class="form-group">
            <label for="taskName">Название</label>
            <input type="text" class="form-control" id="taskName" placeholder="Название" name="name" value="<?= $arTask['FIELDS']['NAME']; ?>">
        </div>
        <div class="form-group">
            <label for="taskStatus">Статус</label>
            <select class="form-control" id="taskStatus" name="status">
                <?php $statusSelect = ($arTask['PROPS']['status']['VALUE_ENUM_ID']) ? $arTask['PROPS']['status']['VALUE_ENUM_ID'] : 1;?>
                <?php foreach ($arStatus as $arItem): ?>
                    <option value="<?= $arItem['ID']; ?>" <?=($statusSelect == $arItem['ID']) ? 'selected' : ''; ?>><?= $arItem['VALUE']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="taskWorkers">Исполнители</label>
            <select multiple class="form-control" id="taskWorkers" name="workers[]">
                <?php $workersSelect = ($arTask['PROPS']['workers']['VALUE']) ? $arTask['PROPS']['workers']['VALUE'] : [];?>
                <?php foreach ($arWorkers as $arItem): ?>
                    <option value="<?= $arItem['ID']; ?>" <?=(in_array($arItem['ID'], $workersSelect)) ? 'selected' : ''; ?>><?= $arItem['NAME']; ?></option>
                <?php endforeach; ?>
            </select>
        </div>
        <div class="form-group">
            <label for="taskDesc">Описание</label>
            <textarea class="form-control" id="taskDesc" rows="3" name="desc"><?= $arTask['FIELDS']['PREVIEW_TEXT']; ?></textarea>
        </div>
        <div class="form-group">
            <p class="form-text text-danger form-text-error"></p>
        </div>
        <button type="submit" class="btn btn-primary">Сохранить</button>
    </form>
</div>