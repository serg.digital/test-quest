$(document).on('submit', '#workerForm', function (e) {
    e.preventDefault();

    var method = 'worker';
    submitForm(method, this);
})

$(document).on('submit', '#taskForm', function (e) {
    e.preventDefault();

    var method = 'task';
    submitForm(method, this);
})

$(document).ready(function() {
    BX.showWait = function () {
        var loader = '<div class="loader"><div class="cssload-clock"></div></div>';
        $('body').append(loader);
    };

    BX.closeWait = function () {
        $('body').find('.loader').remove();
    };
});

function modalOpen (path) {
    $.fancybox.open({
        src: path,
        type: 'ajax'
    });
}

function submitForm(method, form) {
    BX.showWait();
    var data = new FormData(form);

    BX.ajax.runAction(
        'stratosfera:core.api.' + method + '.formHandler',
        {
            data: data
        }
    ).then(function (response) {
        // console.log(response);
        $('.text-danger').text('');

        if (response.data.status) {
            if (response.data.type == 'update') {
                var element = $('[data-id="' + response.data.itemID + '"]');
                element.replaceWith(response.data.html);
            } else if (response.data.type == 'add') {
                var classNameCont = method + '-container';
                var container = $('.' + classNameCont);
                container.append(response.data.html);
            }

            $('.fancybox-close-small').click();
            modalOpen('/local/ajax/modal/success.php');

            // инициализация событий для кнопок управления
            if (method == 'worker') {
                var workers = new JCWorkers({
                    params: {item: response.data.itemID}
                });
            } else if (method == 'task') {
                var tasks = new JCTasks({
                    params: {item: response.data.itemID}
                });
            }
        } else {
            $('.form-text-error').text(response.data.message);
        }
        BX.closeWait();
    });
}