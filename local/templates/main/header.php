<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $USER;
if (!$USER->IsAuthorized()) {
    $USER->Authorize(2);
}
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css">
    <link rel="stylesheet" href="<?=SITE_TEMPLATE_PATH?>/css/custom.css">
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead()?>
</head>
<body>
    <?$APPLICATION->ShowPanel()?>
    <header id="header" class="header bg-light">
        <div class="container">
        </div>
    </header>

    <main id="main" class="main">
        <section class="section my-5 py-2">
            <div class="container">