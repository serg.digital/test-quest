<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["TABLE_HEAD_ID"] = "ID";
$MESS["TABLE_HEAD_NAME"] = "Имя";
$MESS["TABLE_HEAD_POSITION"] = "Должность";
$MESS["TABLE_HEAD_CONTROL"] = "Управление";
$MESS["BTN_TEXT_UPDATE"] = "Изменить";
$MESS["BTN_TEXT_DELETE"] = "Удалить";
$MESS["BTN_TEXT_CREATE"] = "Добавить исполнителя";
?>