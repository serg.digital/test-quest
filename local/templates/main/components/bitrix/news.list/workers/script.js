(function(window) {
    'use strict';

    if (window.JCWorkers)
        return;

    window.JCWorkers = function(arParams) {
        if (typeof arParams === 'object') {
            this.params = arParams.params;
        }

        this.obWorkerCreate = document.querySelector('.workerCreate');
        this.obWorkerUpdate = document.querySelectorAll('.workerUpdate');
        this.obWorkerDelete = document.querySelectorAll('.workerDelete');
        this.initConfig();
    }

    window.JCWorkers.prototype = {
        initConfig: function() {
            if (this.params.item) {
                this.eventAdd(this.params.item);
            } else {
                this.eventAdd();
            }
        },

        eventAdd: function(elem = false) {
            if (elem) {
                var elemUpdate = document.querySelector('tr[data-id="' + elem + '"] .workerUpdate');
                var elemDelete = document.querySelector('tr[data-id="' + elem + '"] .workerDelete');

                if (elemUpdate) {
                    BX.bind(elemUpdate, 'click', BX.delegate(this.workerHandler, this));
                }

                if (elemDelete) {
                    BX.bind(elemDelete, 'click', BX.delegate(this.workerDelete, this));
                }
            } else {
                if (this.obWorkerCreate) {
                    BX.bind(this.obWorkerCreate, 'click', BX.delegate(this.workerHandler, this));
                }

                if (this.obWorkerUpdate) {
                    for (let i = 0; i < this.obWorkerUpdate.length; i++) {
                        BX.bind(this.obWorkerUpdate[i], 'click', BX.delegate(this.workerHandler, this));
                    }
                }

                if (this.obWorkerDelete) {
                    for (let i = 0; i < this.obWorkerDelete.length; i++) {
                        BX.bind(this.obWorkerDelete[i], 'click', BX.delegate(this.workerDelete, this));
                    }
                }
            }
        },

        workerHandler: function() {
            var item = '';
            var target = BX.proxy_context;
            var parent = BX.findParent(target, {attribute: 'data-id'});
            if (parent) {
                item = parent.getAttribute('data-id');
            }

            var path = '/local/ajax/modal/worker.php';
            if (item) {
                path = path + '?item=' + item;
            }
            modalOpen(path);
        },

        workerDelete: function() {
            BX.showWait();
            var target = BX.proxy_context;
            var parent = BX.findParent(target, {attribute: 'data-id'});

            var formData = {
                item: parent.getAttribute('data-id')
            };

            var method = 'delete';
            this.sendAjax(method, formData);
        },

        sendAjax: function (method, formData) {
            BX.ajax.runAction(
                'stratosfera:core.api.worker.' + method,
                {
                    data: formData
                }
            )
            .then(function (response) {
                // console.log(response);
                if (response.data.status) {
                    if (method == 'delete') {
                        var elem = document.querySelector('tr[data-id="' + formData.item + '"]');
                        elem.remove();
                    }
                } else {
                    alert(response.data.message);
                }
                BX.closeWait();
            });
        }
    }
})(window);