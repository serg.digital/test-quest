<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php if (!empty($arResult['ITEMS'])): ?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"><?= GetMessage('TABLE_HEAD_ID'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_NAME'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_POSITION'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_CONTROL'); ?></th>
            </tr>
        </thead>
        <tbody class="worker-container">
            <?php foreach($arResult['ITEMS'] as $arItem): ?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                <tr id="<?= $this->GetEditAreaId($arItem['ID']); ?>" data-id="<?= $arItem['ID']; ?>">
                    <th scope="row"><?= $arItem['ID']; ?></th>
                    <td><?= $arItem['NAME']; ?></td>
                    <td><?= $arItem['PROPERTIES']['position']['VALUE']; ?></td>
                    <td>
                        <a class="btn btn-warning workerUpdate"><?= GetMessage('BTN_TEXT_UPDATE'); ?></a>
                        <a class="btn btn-danger workerDelete"><?= GetMessage('BTN_TEXT_DELETE'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <a class="btn btn-primary workerCreate"><?= GetMessage('BTN_TEXT_CREATE'); ?></a>
<?php endif; ?>

<?$jsParams = [];?>
<script>
    var workers = new JCWorkers({
        params: <?=CUtil::PhpToJSObject($jsParams, false, true)?>,
    });
</script>