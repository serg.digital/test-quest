<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "Будет удалена вся информация, связанная с этой записью. Продолжить?";
$MESS["TABLE_HEAD_ID"] = "ID";
$MESS["TABLE_HEAD_NAME"] = "Название";
$MESS["TABLE_HEAD_WORKERS"] = "Исполнители";
$MESS["TABLE_HEAD_STATUS"] = "Статус";
$MESS["TABLE_HEAD_DESC"] = "Описание";
$MESS["TABLE_HEAD_CONTROL"] = "Управление";
$MESS["BTN_TEXT_UPDATE"] = "Изменить";
$MESS["BTN_TEXT_DELETE"] = "Удалить";
$MESS["BTN_TEXT_CREATE"] = "Добавить задачу";
?>