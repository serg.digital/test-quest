<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<?php if (!empty($arResult['ITEMS'])): ?>
    <table class="table">
        <thead>
            <tr>
                <th scope="col"><?= GetMessage('TABLE_HEAD_ID'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_NAME'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_WORKERS'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_STATUS'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_DESC'); ?></th>
                <th scope="col"><?= GetMessage('TABLE_HEAD_CONTROL'); ?></th>
            </tr>
        </thead>
        <tbody class="task-container">
            <?php foreach($arResult['ITEMS'] as $arItem): ?>
                <?php
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_EDIT'));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem['IBLOCK_ID'], 'ELEMENT_DELETE'), array('CONFIRM' => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $arWorkersName = '';
                if ($arItem['PROPERTIES']['workers']['VALUE']) {
                    $arWorkers = getWorkersInfo($arItem['PROPERTIES']['workers']['VALUE']);
                    $arWorkersName = implode('<br>', array_column($arWorkers, 'NAME'));
                }
                ?>
                <tr id="<?= $this->GetEditAreaId($arItem['ID']); ?>" data-id="<?= $arItem['ID']; ?>">
                    <th scope="row"><?= $arItem['ID']; ?></th>
                    <td><?= $arItem['NAME']; ?></td>
                    <td><?= $arWorkersName; ?></td>
                    <td><?= $arItem['PROPERTIES']['status']['VALUE']; ?></td>
                    <td><?= $arItem['PREVIEW_TEXT']; ?></td>
                    <td>
                        <a class="btn btn-warning taskUpdate"><?= GetMessage('BTN_TEXT_UPDATE'); ?></a>
                        <a class="btn btn-danger taskDelete"><?= GetMessage('BTN_TEXT_DELETE'); ?></a>
                    </td>
                </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <a class="btn btn-primary taskCreate"><?= GetMessage('BTN_TEXT_CREATE'); ?></a>
<?php endif; ?>

<?$jsParams = [];?>
<script>
    var tasks = new JCTasks({
        params: <?=CUtil::PhpToJSObject($jsParams, false, true)?>,
    });
</script>
