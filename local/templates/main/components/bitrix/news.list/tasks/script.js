(function(window) {
    'use strict';

    if (window.JCTasks)
        return;

    window.JCTasks = function(arParams) {
        if (typeof arParams === 'object') {
            this.params = arParams.params;
        }

        this.obTaskCreate = document.querySelector('.taskCreate');
        this.obTaskUpdate = document.querySelectorAll('.taskUpdate');
        this.obTaskDelete = document.querySelectorAll('.taskDelete');
        this.initConfig();
    }

    window.JCTasks.prototype = {
        initConfig: function() {
            if (this.params.item) {
                this.eventAdd(this.params.item);
            } else {
                this.eventAdd();
            }
        },

        eventAdd: function(elem = false) {
            if (elem) {
                var elemUpdate = document.querySelector('tr[data-id="' + elem + '"] .taskUpdate');
                var elemDelete = document.querySelector('tr[data-id="' + elem + '"] .taskDelete');

                if (elemUpdate) {
                    BX.bind(elemUpdate, 'click', BX.delegate(this.taskHandler, this));
                }

                if (elemDelete) {
                    BX.bind(elemDelete, 'click', BX.delegate(this.taskDelete, this));
                }
            } else {
                if (this.obTaskCreate) {
                    BX.bind(this.obTaskCreate, 'click', BX.delegate(this.taskHandler, this));
                }

                if (this.obTaskUpdate) {
                    for (let i = 0; i < this.obTaskUpdate.length; i++) {
                        BX.bind(this.obTaskUpdate[i], 'click', BX.delegate(this.taskHandler, this));
                    }
                }

                if (this.obTaskDelete) {
                    for (let i = 0; i < this.obTaskDelete.length; i++) {
                        BX.bind(this.obTaskDelete[i], 'click', BX.delegate(this.taskDelete, this));
                    }
                }
            }
        },

        taskHandler: function() {
            var item = '';
            var target = BX.proxy_context;
            var parent = BX.findParent(target, {attribute: 'data-id'});
            if (parent) {
                item = parent.getAttribute('data-id');
            }

            var path = '/local/ajax/modal/task.php';
            if (item) {
                path = path + '?item=' + item;
            }
            modalOpen(path);
        },

        taskDelete: function() {
            BX.showWait();
            var target = BX.proxy_context;
            var parent = BX.findParent(target, {attribute: 'data-id'});

            var formData = {
                item: parent.getAttribute('data-id')
            };

            var method = 'delete';
            this.sendAjax(method, formData);
        },

        sendAjax: function (method, formData) {
            BX.ajax.runAction(
                'stratosfera:core.api.task.' + method,
                {
                    data: formData
                }
            )
            .then(function (response) {
                // console.log(response);
                if (response.data.status) {
                    if (method == 'delete') {
                        var elem = document.querySelector('tr[data-id="' + formData.item + '"]');
                        elem.remove();
                    }
                } else {
                    alert(response.data.message);
                }
                BX.closeWait();
            });
        }
    }
})(window);