<?php

$menuItems = [
    [
        'parent_menu' => 'global_stratosfera_settings',
        'sort' => 100,
        'text' => 'Настройки сайтов',
        'title' => 'Настройки сайтов',
        'url' => 'stratosfera_site_settings.php?lang=' . LANG,
        'more_url' => [
            'stratosfera_site_settings.php'
        ]
    ]
];

//return $menuItems;
