<?php


namespace Stratosfera\Core\Internal;


use Bitrix\Main\Loader;
use Stratosfera\Core\Events\Manager;


class Events
{
    public static function onPageStart() {
        // Загружаем модуль для доступности на сайте + регистрируем события
        Loader::includeModule('stratosfera.core');
        Manager::registerEvents();
    }

}
