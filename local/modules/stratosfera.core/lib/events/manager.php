<?php


namespace Stratosfera\Core\Events;


use Bitrix\Main\EventManager;

class Manager
{
    public static function registerEvents()
    {
        $eventManager = EventManager::getInstance();

        $eventManager->addEventHandler(
            'iblock',
            'OnAfterIBlockElementAdd',
            ['\Stratosfera\Core\Events\Iblock', 'OnAfterIBlockElementHandler']
        );

        $eventManager->addEventHandler(
            'iblock',
            'OnAfterIBlockElementUpdate',
            ['\Stratosfera\Core\Events\Iblock', 'OnAfterIBlockElementHandler']
        );
    }
}
