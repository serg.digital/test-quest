<?php


namespace Stratosfera\Core\Events;


use \Stratosfera\Core\Helpers\Iblock as IBHelper;

class Iblock
{
    public static function OnAfterIBlockElementHandler(&$arFields) {

        $IB_TASKS_ID = IBHelper::getIblockIdByCode(IB_CODE_TASKS);
        if ($arFields['IBLOCK_ID'] == $IB_TASKS_ID) {
            $arTasks = [];
            $rsTasks = \CIBlockElement::GetList(
                [],
                [
                    'IBLOCK_ID' => $IB_TASKS_ID,
                    'ID' => $arFields['ID']
                ],
                false,
                false,
                ['ID', 'PROPERTY_workers']
            );
            while ($arRes = $rsTasks->fetch()) {
                $arTasks[$arRes['PROPERTY_WORKERS_VALUE']] = $arRes['PROPERTY_WORKERS_VALUE'];
            }

            $propValue = (count($arTasks) == 1) ? 4 : false;
            \CIBlockElement::SetPropertyValuesEx($arFields['ID'], $IB_TASKS_ID, array('check_worker' => $propValue));
        }
    }
}
