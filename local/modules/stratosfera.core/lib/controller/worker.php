<?php


namespace Stratosfera\Core\Controller;


use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;

class Worker extends Controller
{
    public function configureActions() {
        return [
            'add' => ['PREFILTERS' => []]
        ];
    }

    public function formHandlerAction() {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $post = $request->getPostList()->toArray();

        $status = false;
        $errors = [];
        $message = 'Исполнитель добавлен';
        $type = 'add';
        $html = '';
        $itemID = '';

        if (!$post['name']) $errors[] = 'Укажите имя';

        if (empty($errors)) {
            Loader::IncludeModule('iblock');
            $IB_WORKERS_ID = \Stratosfera\Core\Helpers\Iblock::getIblockIdByCode(IB_CODE_WORKERS);

            $worker = new \CIBlockElement;
            $arFields = [
                'NAME' => htmlspecialchars($post['name']),
                'IBLOCK_ID' => $IB_WORKERS_ID,
                'PROPERTY_VALUES' => [
                    'position' => htmlspecialchars($post['position'])
                ]
            ];

            if ($post['item']) {
                $res = $worker->Update($post['item'], $arFields);
                $message = 'Исполнитель обновлен';
                $type = 'update';
            } else {
                $res = $worker->Add($arFields);
            }

            if (!$res) {
                $errors[] = $worker->LAST_ERROR;
            }
        }

        if (empty($errors)) {
            $status = true;
            $itemID = ($post['item']) ? $post['item'] : $res;
            $html = Worker::getHtml($itemID, $post);
        } else {
            $message = implode(', ', $errors);
        }

        $result = [
            'status' => $status,
            'errors' => $errors,
            'message' => $message,
            'type' => $type,
            'html' => $html,
            'itemID' => $itemID
        ];
        return $result;
    }

    public function deleteAction() {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $post = $request->getPostList()->toArray();
        $allow = Worker::checkDelete($post['item']);

        $status = false;
        $errors = [];

        if ($allow) {
            $message = 'Исполнитель удален';

            Loader::IncludeModule('iblock');
            $res = \CIBlockElement::Delete($post['item']);

            if (!$res) {
                $errors[] = 'Ошибка при удалении';
            }

            if (empty($errors)) {
                $status = true;
            } else {
                $message = implode('<br>', $errors);
            }
        } else {
            $message = 'Исполнитель имеет задачу где является единственным исполнителем';
        }

        $result = [
            'status' => $status,
            'errors' => $errors,
            'message' => $message,
        ];

        return $result;
    }

    public static function getHtml($itemID, $post) {
        return '
            <tr data-id="'.$itemID.'">
                <th scope="row">'.$itemID.'</th>
                <td>'.$post['name'].'</td>
                <td>'.$post['position'].'</td>
                <td>
                    <a class="btn btn-warning workerUpdate">Изменить</a>
                    <a class="btn btn-danger workerDelete">Удалить</a>
                </td>
            </tr>
        ';
    }

    /**
     * Проверка для предотвращения удаления исполнителя, который имеет задачу - где является единственным исполнителем
     *
     * @param int $workerID id исполнителя, которого надо проверить
     * @return boolean false, если удаление запрещено и true - если разрешено
     **/
    public static function checkDelete($workerID) {
        $IB_TASKS_ID = \Stratosfera\Core\Helpers\Iblock::getIblockIdByCode(IB_CODE_TASKS);

        $arTasks = [];
        $rsTasks = \CIBlockElement::GetList(
            [],
            [
                'IBLOCK_ID' => $IB_TASKS_ID,
                'PROPERTY_WORKERS' => $workerID,
                '!PROPERTY_check_worker' => false
            ],
            false,
            false,
            ['ID']
        );
        while ($arRes = $rsTasks->fetch()) {
            $arTasks[$arRes['ID']] = $arRes['ID'];
        }

        return (!empty($arTasks)) ? false : true;
    }
}