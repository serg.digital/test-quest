<?php


namespace Stratosfera\Core\Controller;


use Bitrix\Main\Engine\Controller;
use Bitrix\Main\Loader;
use \Stratosfera\Core\Helpers\Iblock;

class Task extends Controller
{
    public function configureActions() {
        return [
            'add' => ['PREFILTERS' => []]
        ];
    }

    public function formHandlerAction() {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $post = $request->getPostList()->toArray();

        $status = false;
        $errors = [];
        $message = 'Задача добавлена';
        $type = 'add';
        $html = '';
        $itemID = '';

        $post = Task::clearParam($post);
        if (!$post['name']) $errors[] = 'Укажите название';

        if (empty($errors)) {
            Loader::IncludeModule('iblock');
            $IB_TASKS_ID = Iblock::getIblockIdByCode(IB_CODE_TASKS);

            $worker = new \CIBlockElement;
            $arFields = [
                'NAME' => $post['name'],
                'IBLOCK_ID' => $IB_TASKS_ID,
                'PREVIEW_TEXT' => $post['desc'],
                'PROPERTY_VALUES' => [
                    'workers' => $post['workers'],
                    'status' => ['VALUE' => $post['status']]
                ]
            ];

            $arStatus = \CIBlockPropertyEnum::GetByID($post['status']);
            $arWorkers = [];
            if ($post['workers']) {
                $arWorkers = getWorkersInfo($post['workers']);
                $arWorkers = array_column($arWorkers, 'NAME');
            }

            if ($post['item']) {
                $res = $worker->Update($post['item'], $arFields);
                $message = 'Задача обновлена';
                $type = 'update';
            } else {
                $res = $worker->Add($arFields);
            }

            if (!$res) {
                $errors[] = $worker->LAST_ERROR;
            }
        }

        if (empty($errors)) {
            $status = true;
            $itemID = ($post['item']) ? $post['item'] : $res;
            $html = Task::getHtml($itemID, $post, $arWorkers, $arStatus);
        } else {
            $message = implode('<br>', $errors);
        }

        $result = [
            'status' => $status,
            'errors' => $errors,
            'message' => $message,
            'type' => $type,
            'html' => $html,
            'itemID' => $itemID
        ];
        return $result;
    }

    public function deleteAction() {
        $request = \Bitrix\Main\Context::getCurrent()->getRequest();
        $post = $request->getPostList()->toArray();

        $status = false;
        $errors = [];
        $message = 'Задача удалена';

        Loader::IncludeModule('iblock');
        $res = \CIBlockElement::Delete($post['item']);

        if (!$res) {
            $errors[] = 'Ошибка при удалении';
        }

        if (empty($errors)) {
            $status = true;
        } else {
            $message = implode('<br>', $errors);
        }

        $result = [
            'status' => $status,
            'errors' => $errors,
            'message' => $message,
        ];
        return $result;
    }

    public static function clearParam($param) {
        foreach ($param as $key => $val) {
            if (is_array($val)) continue;
            $param[$key] = htmlspecialchars(trim($val));
        }
        return $param;
    }

    public static function getHtml($itemID, $post, $arWorkers, $arStatus) {
        return '
            <tr data-id="'.$itemID.'">
                <th scope="row">'.$itemID.'</th>
                <td>'.$post['name'].'</td>
                <td>'.implode('<br>', $arWorkers).'</td>
                <td>'.$arStatus['VALUE'].'</td>
                <td>'.$post['desc'].'</td>
                <td>
                    <a class="btn btn-warning taskUpdate">Изменить</a>
                    <a class="btn btn-danger taskDelete">Удалить</a>
                </td>
            </tr>
        ';
    }
}