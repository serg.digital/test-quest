<?php
return [
    'controllers' => [
        'value' => [
            'namespaces' => [
                '\\Stratosfera\\Core\\Controller' => 'api'
            ]
        ],
        'readonly' => true
    ]
];